#include <stdio.h>

double triple(double);

int main(int argc, char const *argv[])
{

    printf("Input a number:");
    double num;
    scanf("%lf", &num);

    printf("Triple %.1lf is %.1lf.\n", num, triple(num));

    return 0;
}

double triple(double num)
{
    return num * num * num;
}