#include <stdio.h>

int main(int argc, char const *argv[])
{
    while (1)
    {
        int i = 1;
        printf("Input a time(minute): ");
        scanf("%d", &i);
        if (i <= 0)
        {
            break;
        }
        printf("%d minute is %d hours and %d minute.\n", i, i / 60, i % 60);
    }
    return 0;
}
