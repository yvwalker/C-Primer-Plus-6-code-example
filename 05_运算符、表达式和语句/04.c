#include <stdio.h>
#define FEET 30.48
#define INCHE 2.54

int main(int argc, char const *argv[])
{

    while (1)
    {
        printf("Enter a height of centimeters: ");
        double height;
        scanf("%lf", &height);

        if (height <= 0)
        {
            printf("bye\n");
            break;
        }
        printf("%.1lf cm = %d feet, %.1lf inchs\n", height, (int)(height / FEET), height / INCHE);
    }
    return 0;
}
