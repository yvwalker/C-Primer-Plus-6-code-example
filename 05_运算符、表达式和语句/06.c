#include <stdio.h>

int main(int argc, char const *argv[])
{
    while (1)
    {
        int count, sum;

        count = 1;
        sum = 0;

        printf("Input a day: ");
        int days;
        scanf("%d", &days);

        while (count <= days)
        {
            sum = sum + count * count;
            count++;
        }
        printf("sum = %d\n", sum);
    }
    return 0;
}
