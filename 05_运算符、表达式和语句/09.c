#include <stdio.h>

void Temperatures(double);
int main(int argc, char const *argv[])
{

    double H;

    printf("Input the first temperatures:");
    while (scanf("%lf", &H))
    {
        Temperatures(H);
    }
    return 0;
}

void Temperatures(double H)
{
    const double C = 5.0 / 9.0;
    const double K = 273.16;
    printf("%lf = %lf = %lf\n", H, C * (H - 32.0), H + 273.16);
    printf("Input next temperatures:");
}