#include <stdio.h>

int main(int argc, char const *argv[])
{
    while (1)
    {
        printf("Input num of days: ");
        int day;
        scanf("%d", &day);

        if (day <= 0)
        {
            break;
        }
        printf("%d days is %d weeks, %d days.\n", day, day / 7, day % 7);
    }

    return 0;
}
