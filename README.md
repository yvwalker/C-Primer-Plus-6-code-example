# C-Primer-Plus-6-code-example

## 项目介绍
C Primer Plus 第六版 编程练习实践。每个程序都会做测试。

#### 不断更新中...

## 索引

1. 初识C语言
2. [C语言概述](https://gitee.com/keyvuui/C-Primer-Plus-6-code-example/tree/master/02_C语言概述)
3. [数据和C](https://gitee.com/keyvuui/C-Primer-Plus-6-code-example/tree/master/03_数据和C)
4. [字符串的格式化输入和输出](https://gitee.com/keyvuui/C-Primer-Plus-6-code-example/tree/master/04_字符串和格式化输入输出)
