#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    printf("Input your lastname: ");
    char ln[40];
    scanf("%s", ln);
    printf("Input your firstname: ");
    char fn[40];
    scanf("%s", fn);

    strcat(ln, fn);
    printf("%s\n", ln);
    printf("%20s\n", ln);
    printf("%-20s\n", ln);
    char space[10];
    printf("%3s%s\n", space, ln);

    return 0;
}
