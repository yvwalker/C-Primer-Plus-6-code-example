#include <stdio.h>

int main(int argc, char const *argv[])
{
    float height;
    char name[40];
    printf("Input your height(cm): ");
    scanf("%f", &height);
    printf("Input your name: ");
    scanf("%s", name);

    printf("%s, you are %.3f m tall.\n",
           name, height / 100);
    return 0;
}
