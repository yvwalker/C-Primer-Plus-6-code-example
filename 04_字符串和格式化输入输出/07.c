#include <stdio.h>
#include <float.h>

int main(int argc, char const *argv[])
{
    double dou = 1.0 / 3.0;
    float flo = 1.0 / 3.0;
    printf("double: \n%.6lf %.12lf %.16lf\nfloat: \n%.6f %.12f %.16f\nFLT_DIG: %d, DBL_DIG: %d\n",
           dou, dou, dou, flo, flo, flo, FLT_DIG, DBL_DIG);
    return 0;
}
