#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    float any;
    printf("Input a float value: ");
    scanf("%f", &any);

    printf("%f = %E\n", any, any);

    return 0;
}
