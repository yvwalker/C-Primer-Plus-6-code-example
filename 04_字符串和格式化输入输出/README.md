# 要点总结

## strcat

&emsp;&emsp;**strcat** 定义在 **string.h** 函数中，其函数原型为：

```c
char *strcat( char *dest, const char *src );
```

&emsp;&emsp;先看一下标准库中对 **strcat** 的解释：

>  Appends a copy of the null-terminated byte string pointed to by `src` to the end of the null-terminated byte string pointed to by `dest`. The character `src[0]` replaces the null terminator at the end of `dest`. The resulting byte string is null-terminated.
>
>   The behavior is undefined if the destination array does not have enough space for the contents of both `dest` and the first `count` characters of `src`, plus the terminating null character. 
>
>   The behavior is undefined if the source and destination objects overlap. 
>
>   The behavior is undefined if either `dest` is not a pointer to a null-terminated byte string or `src` is not a pointer to a character array,

&emsp;&emsp;这一大段文字是说这个函数是将 **src** 拼接在 **dest** 的最后。**src** 的第一个字符会取代 **dest** 最后的 **null** 字符。整个拼接后的字符串以 **null** 做结束。

&emsp;&emsp;当然，如果 **dest** 的空间不够存放拼接的字符串，或者传入的 **dest** 或者 **src** 类型有错，甚至是 **dest** 和 **src** 有部分重叠，都会不能通过编译( The behavior is undefined )。

## float 丢失精度



## 格式化输出偏移量