#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    printf("Input your first name: ");
    char fn[40];
    scanf("%s", fn);
    printf("Input your last name: ");
    char ln[40];
    scanf("%s", ln);

    printf("%10s %10s\n", fn, ln);
    printf("%10d %10d\n", (int)strlen(fn), (int)strlen(ln));
    printf("%-10s %-10s\n", fn, ln);
    printf("%-10d %-10d\n", (int)strlen(fn), (int)strlen(ln));
    return 0;
}
