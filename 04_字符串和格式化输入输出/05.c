#include <stdio.h>

int main(int argc, char const *argv[])
{
    printf("Input your speed(Mb/s): ");
    float speed;
    scanf("%f", &speed);

    printf("Input your file size(MB): ");
    float size;
    scanf("%f", &size);

    printf("At %.2f megabits per second, a file of %.2f megabytes\ndownloads in %.2f seconds.\n",
           speed, size, size * 8 / speed);

    return 0;
}
