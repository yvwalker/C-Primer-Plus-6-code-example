#include <stdio.h>

#define JIA_LUN 3.785
#define MILE 1.609

int main(int argc, char const *argv[])
{
    printf("Input your mile:");
    double mile;
    scanf("%lf", &mile);

    printf("Input your jialun:");
    double jialun;
    scanf("%lf", &jialun);

    printf("MAIL/JIA_LUN: %.1lf/%.1lf = L/KILO: %.1lf/%.1lf\n",
           mile, jialun, 100 * (jialun * JIA_LUN) / mile * MILE, 100.0);

    return 0;
}
