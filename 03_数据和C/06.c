#include <stdio.h>

int main(int argc, char const *argv[])
{
    printf("Input the kua of a water: ");
    double kua;
    scanf("%lf", &kua);

    double per = 3e-23;
    double oneKua = 950;
    printf("Kua is %lf, per water is %lf \n", kua, (kua * oneKua) / per);
    return 0;
}
