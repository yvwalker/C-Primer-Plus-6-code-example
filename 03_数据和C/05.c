#include <stdio.h>

int main(int argc, char const *argv[])
{
    printf("Please input your age: ");
    int age = 0;
    scanf("%d", &age);

    float second = 3.156e+07;

    printf("You have been through %f second.\n", second * age);
    return 0;
}