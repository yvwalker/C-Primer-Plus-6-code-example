#include <stdio.h>

int main(int argc, char const *argv[])
{
    printf("Input your highet: ");
    double high = 0;
    scanf("%lf", &high);

    double oneInch = 30.48;

    printf("Your length is %lfinch, %lfcm\n", high, high * oneInch);
    return 0;
}
