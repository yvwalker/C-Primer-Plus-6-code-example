#include <stdio.h>

int main(int argc, char const *argv[])
{
    char word[26];

    for (int i = 0; i < 26; i++)
    {
        word[i] = 'a' + i;
        printf("%c", word[i]);
    }
    printf("\n");

    return 0;
}
