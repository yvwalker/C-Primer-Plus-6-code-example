#include <stdio.h>

int main(int argc, char const *argv[])
{
    char init = 'A';
    for (int i = 0; i < 6; i++)
    {
        for (int j = 0; j < i + 1; j++)
        {
            printf("%c", init + j);
        }
        init = init + i + 1;
        printf("\n");
    }
    return 0;
}
