#include <stdio.h>

int main(int argc, char const *argv[])
{
    printf("Input a word:");
    char word;
    scanf("%c", &word);

    for (char i = 'A'; i <= word; i++)
    {
        for (int space = 0; space < word - i; space++)
        {
            printf(" ");
        }
        for (char k = 'A'; k < i; k++)
        {
            printf("%c", k);
        }
        for (char l = i; l >= 'A'; l--)
        {
            printf("%c", l);
        }
        printf("\n");
    }
    return 0;
}
