# 要点总结

### 1. printf

&emsp;&emsp;**printf** 函数可以格式化输出变量，**printf** 是 **print format** 的简写。顾名思义，我们可以对这个函数指定相应的输出格式。并且一般都为所表示的类型的英文首字母，例如：

%d -> digital, %s -> string, %p -> point, %c -> char ...... 等等诸如此类很多很多。

## 注：如果 **printf** 的字符串之后不加 \n，则终端中的输出会有乱码字符 %。