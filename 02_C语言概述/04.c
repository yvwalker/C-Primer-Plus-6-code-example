#include <stdio.h>

void jolly()
{
    int i;
    for (i = 0; i < 3; i++)
    {
        printf("For he's a jolly fellow!\n");
    }
}

void deny()
{
    printf("Which nobody can deny\n");
}

int main(int argc, char const *argv[])
{
    jolly();
    deny();

    return 0;
}
