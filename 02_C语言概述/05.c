#include <stdio.h>

void br()
{
    printf("Brazil, Russia, India, China\n");
}

void ic()
{
    printf("India, China\n");
}

int main(int argc, char const *argv[])
{
    br();
    ic();
    printf("Brazil, Russia\n");
    return 0;
}
