#include <stdio.h>

void two()
{
    printf("two\n");
}

void one_three()
{
    printf("one\n");
    two();
    printf("three\n");
}

int main(int argc, char const *argv[])
{
    printf("Starting now: \n");
    one_three();
    printf("Done! \n");

    return 0;
}
